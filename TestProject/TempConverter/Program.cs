﻿// See https://aka.ms/new-console-template for more information

public class TempConverter
{
    public  static void Main() { }

    public static double CelciousToFarenheit(double temp)
    {
        return (temp * 1.8) + 32;
    }

    public static double FarenheitToCelcious(double temp)
    {
        return (temp - 32) * 1.8;
    }
}