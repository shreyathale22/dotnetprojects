namespace TestProject1
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            //Initialization or arrange
            var temp1 = 40;
            var expected = 104;

            //Act
            //Call the development code

            var actual = TempConverter.CelciousToFarenheit(temp1);

            //Assert
            Assert.Equal(expected, actual); ;
        }

        [Fact]
        public void Test2()
        {
            //Initialization or arrange
            var temp1 = 40;
            var expected = 14.4;

            //Act
            //Call the development code

            var actual = TempConverter.FarenheitToCelcious(temp1);

            //Assert
            Assert.Equal(expected, actual);
        }
    }
}