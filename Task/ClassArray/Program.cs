﻿// See https://aka.ms/new-console-template for more information
using ClassArray.Model;
using ClassArray.Repository;

ProductRepository projectRepo = new ProductRepository();

Console.WriteLine("------------Get All Products----------------");
Product[] Allproducts = projectRepo.GetAllproducts();
foreach (Product item in Allproducts)
{
    Console.WriteLine(item);
}

Console.WriteLine("------------Get Products By Category----------------");
var a = projectRepo.GetProductByCat("Mobile");
foreach (Product p in a)
{
    Console.WriteLine(p);
}

Console.WriteLine("------------Delete Product----------------");
Console.WriteLine("Enter Id which you want to delete");
int delid = int.Parse(Console.ReadLine());
var del = projectRepo.DeleteArray(delid);

foreach (Product item in del)
{
    Console.WriteLine(item);

}

Console.WriteLine("------------Update Product----------------");
Console.WriteLine("Enter the Id whose name you want to update");
int update = int.Parse(Console.ReadLine());

Console.WriteLine("Enter the value");
string value = Console.ReadLine();

var data1 = projectRepo.UpdateArray(update, value);

foreach (Product item in data1)
{
    item.Name = value;
    Console.WriteLine(item);
}



