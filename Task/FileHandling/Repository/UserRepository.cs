﻿using FileHandling.Exception;
using FileHandling.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileHandling.Repository
{

    internal class UserRepository : IUserRepository, IFile
    {
        List<User> users;
        public UserRepository()
        {
            users = new List<User>();
        }

        public bool RegisterUser(User user)
        {
            users.Add(user);
            string filename = @"D:\DotNetTraining\Day 5\FileHandling\bin\Debug\net6.0\user.txt";
            bool result = IsUserNameExists(user.Name, filename);
            if (result)
            {
                return false;
            }
            else
            {
                WriteContentsToFile(user, filename);
                Console.WriteLine("Register Sucessfully ");
                return true;
            }
        }

        public bool IsUserNameExists(string userName, string filename)
        {
            bool IsUserAvailable = false;
            using (StreamReader sr = new StreamReader(filename))
            {
                string rowline;

                while ((rowline = sr.ReadLine()) != null)
                {
                    string[] rowSplittedValues = rowline.Split(',');
                    foreach (string value in rowSplittedValues)
                    {
                        if (value == userName)
                        {
                            IsUserAvailable = true;
                            throw new Invaliduser("Sorry, Username already exist");
                            break;

                        }
                    }
                }
            }
            return IsUserAvailable;
        }
        public List<string> ReadContentsFromFile(string filename)
        {
            List<string> rowValues = new List<string>();

            using (StreamReader sr = new StreamReader(filename))
            {
                string rowLine;
                while ((rowLine = sr.ReadLine()) != null)
                {
                    rowValues.Add(rowLine);
                }
                return rowValues;
            }
        }

        public void WriteContentsToFile(User user, string filename)
        {
            using (StreamWriter sw = new StreamWriter(filename, true))
            {
                sw.WriteLine($"{user}");
            }

        }
    }
}
