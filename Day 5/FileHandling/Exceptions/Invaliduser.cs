﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileHandling.Exception
{
    internal class Invaliduser : IOException
    {
        public Invaliduser(string message) : base(message)
        {

        }
    }
}
