﻿using ClassArray.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassArray.Repository
{
    internal class ProductRepository
    {
        Product[] Products;
        public ProductRepository()
        {
            Products = new Product[]
            {
                new Product(1,"oppo", "Mobile", 20000, 4.2f),
                new Product(2,"Iphone", "Mobile", 20000, 4.2f),
                new Product(3,"Lenovo", "Laptop", 20000, 4.2f),
                new Product(4,"Samsung", "Tv", 20000, 4.2f)
            };
        }



        public Product[] GetAllproducts()
        {
            return Products;
        }

        public Product[] GetProductByCat(string category)
        {
            return Array.FindAll(Products, Product => Product.Category == category);

        }
        public Product[] DeleteArray(int id)
        {
            Console.WriteLine("delete Element by id");
            return Array.FindAll(Products, Product => Product.Id != id);
        }
        public Product[] UpdateArray(int id, string value)
        {
            return Array.FindAll(Products, product => product.Id == id);
        }

    }
}
