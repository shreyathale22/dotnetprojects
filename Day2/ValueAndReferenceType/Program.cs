﻿// See https://aka.ms/new-console-template for more information


#region Value Type
using ValueAndReferenceType;

Console.WriteLine("Value Type");
Console.WriteLine("Before");
int num1 = 20;
int num2 = 20;
Console.WriteLine($"The value of num1 is {num1}");
Console.WriteLine($"The value of num2 is {num2}");
Console.WriteLine("After");
num2 = 150;
Console.WriteLine($"The value of num1 is {num1}");
Console.WriteLine($"The value of num2 is {num2}");
#endregion

Console.WriteLine("Reference Type");
Console.WriteLine("Before");
Demo obj = new Demo();
Demo obj1 = obj;
Console.WriteLine($"Width is {obj.width}");
Console.WriteLine($"Width is {obj1.width}");
obj1.width = 30;
Console.WriteLine("After");
Console.WriteLine($"Width is {obj.width}");
Console.WriteLine($"Width is {obj1.width}");

