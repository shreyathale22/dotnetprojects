﻿// See https://aka.ms/new-console-template for more information

using Structure;

Customer CustomerObj = new Customer();

CustomerObj.Cus_id = 1;
CustomerObj.Cus_name = "Shreya";


Product ProductObj = new Product(11, "OnePlus", 45000);
CustomerObj.Cus_product = ProductObj;

Console.WriteLine($"Customer Id: {CustomerObj.Cus_id} /t Customer Name: {CustomerObj.Cus_name} /t Product Id: {CustomerObj.Cus_product.ProductId} Product Name: {CustomerObj.Cus_product.ProductName}");

