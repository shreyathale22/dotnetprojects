﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Structure
{
    internal class Customer
    {
        public int Cus_id;
        public string Cus_name;
        public Product Cus_product;
    }
}
