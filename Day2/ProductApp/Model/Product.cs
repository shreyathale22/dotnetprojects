﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductApp.Model
{
    internal class Product
    {
        public int ProductId;
        public string ProductName;
        public int ProductPrice;

        public Product(int pid, string pname, int pprice)
        {
            this.ProductId = pid;
            this.ProductName = pname;
            this.ProductPrice = pprice;

        }
    }
}
