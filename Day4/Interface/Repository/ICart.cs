﻿using Interface.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface.Repository
{
    internal interface ICart
    {
        string AddToCart(Product product); 
        List<Product> DeleteProduct(int index);

        string CancelOrder();
    }
}
