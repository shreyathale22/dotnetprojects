﻿using Interface.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface.Repository
{
    internal class ProductRepository : IOrder, ICart
    {
        List<Product> products;

        public ProductRepository()
        {
            products = new List<Product>();
        }
        public string AddToCart(Product product)
        {
            //add item to list
            products.Add(product);
            return $"Product added successfully";
        }

        public List<Product> BookOrder()
        {
            return products;
        }

        public string CancelOrder()
        {
            return $"IOrder Order Cancelled";
        }

        public List<Product> DeleteProduct(int index)
        {
            products.RemoveAt(index); 
            return products;
        }

        string ICart.CancelOrder()
        {
            return "Icart Order Cancelled";
        }

       
    }
}
