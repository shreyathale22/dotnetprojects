﻿// See https://aka.ms/new-console-template for more information

using Interface.Model;
using Interface.Repository;
ProductRepository productRepo = new ProductRepository();

//IOrder order = (IOrder) productRepo;
//order.BookOrder();

//Product Instance
Product productObj = new Product() {Name = "Mobile", Price = 12000};
var cartStatus = productRepo.AddToCart(productObj);
Console.WriteLine(cartStatus);

//Add second item
productObj = new Product() { Name = "TV", Price = 22000 };
var cartStatus1 = productRepo.AddToCart(productObj);
Console.WriteLine(cartStatus1);

//call to booking order method
List<Product> orderDetails = productRepo.BookOrder();
foreach(var order in orderDetails)
{
    Console.WriteLine($"Name :: {order.Name} \t Price :: {order.Price}");
}

//call to cancel order method
string cancelOrder =  productRepo.CancelOrder();
Console.WriteLine(cancelOrder);


//delete product

Console.WriteLine("Enter the index");
var index = int.Parse(Console.ReadLine());
List<Product> deleteList = productRepo.DeleteProduct(index);
foreach (var del in deleteList)
{
    Console.WriteLine($"Name :: {del.Name} \t Price :: {del.Price}");
}


//ICart obj = (ICart) productRepo;
//Console.WriteLine(obj.CancelOrder()); 
