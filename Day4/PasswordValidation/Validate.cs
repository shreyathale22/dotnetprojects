﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace PasswordValidation
{

    public static class Validate
    {
        public const string pattern = @"^[A-Z][a-z]{8}[@#$%]+$";

        public static bool validatePassword(string num)
        {
            if (num != null) return Regex.IsMatch(num, pattern);
            else return false;
        }
    }
}
