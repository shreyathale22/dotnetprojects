﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymorphism
{
    internal class Book
    {
        public void viewBooks(string name, string author)
        {
            Console.WriteLine($"Book Details =  BookName : {name} \t Author : {author}");
        }

        public void viewBooks(string name, string author, int price)
        {
            Console.WriteLine($"Book Details =  BookName : {name} \t Author : {author} \t Price : {price}");
        }
    }
}
